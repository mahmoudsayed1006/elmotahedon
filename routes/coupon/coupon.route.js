import express from 'express';
import CouponController from '../../controllers/coupon/coupon.controller';
import { requireAuth } from '../../services/passport';
import { multerSaveTo } from '../../services/multer-service';

const router = express.Router();

router.route('/')
    .post(
        requireAuth,
        CouponController.validateBody(),
        CouponController.create
    )
    .get(CouponController.findAll);
    
router.route('/:CouponId')
    .put(
        requireAuth,
        CouponController.validateBody(true),
        CouponController.update
    )
    .get(CouponController.findById)
    .delete( requireAuth,CouponController.delete);

    router.route('/:couponId/end')
    .put(
        requireAuth,
        CouponController.end
    )
    router.route('/:couponId/reused')
    .put(
        requireAuth,
        CouponController.reused
    )





export default router;