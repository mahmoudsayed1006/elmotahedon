import express from 'express';
import { requireSignIn, requireAuth } from '../../services/passport';
import UserController from '../../controllers/user/user.controller';
import { multerSaveTo } from '../../services/multer-service';

const router = express.Router();


router.post('/signin', UserController.signIn);
router.post('/socialLogin', UserController.socialLogin);

router.route('/signup')
    .post(
        multerSaveTo('users').single('img'),
        UserController.validateUserCreateBody(),
        UserController.signUp
    );
router.route('/addSalesMan')
    .post(
        requireAuth,
        multerSaveTo('users').fields([
            { name: 'img', maxCount: 1, options: false },
            { name: 'transportLicense', maxCount: 8, options: false },
            { name: 'transportImages', maxCount: 4, options: false }

        ]),

        UserController.validateSalesManCreateBody(),
        UserController.addSalesMan
    );
router.route('/becameSalesMan')
    .put(
        requireAuth,
        multerSaveTo('users').fields([
            { name: 'transportLicense', maxCount: 8, options: false },
            { name: 'transportImages', maxCount: 4, options: false }

        ]),
        UserController.becameSalesMan
       
    )
router.route('/:userId/active')
    .put(
        requireAuth,
        UserController.active
    );
router.route('/:userId/block')
    .put(
        requireAuth,
        UserController.block
    );

    router.route('/:userId/unblock')
    .put(
        requireAuth,
        UserController.unblock
    );
router.route('/:userId/disactive')
    .put(
        requireAuth,
        UserController.disactive
    );
router.route('/logout')
    .post(
        requireAuth,
        UserController.logout
    );
router.route('/addToken')
    .post(
        requireAuth,
        UserController.addToken
    );
router.route('/updateToken')
    .put(
        requireAuth,
        UserController.updateToken
    );


router.put('/check-exist-email', UserController.checkExistEmail);

router.put('/check-exist-phone', UserController.checkExistPhone);

router.put('/user/updateInfo',
    requireAuth,
    multerSaveTo('users').single('img'),
    UserController.validateUpdatedBody(),
    UserController.updateInfo);

router.put('/user/:userId/updateInfo',
    requireAuth,
    multerSaveTo('users').single('img'),
    UserController.validateUpdatedBodyAdmin(),
    UserController.updateInfoAdmin);

router.post('/sendCode',
    UserController.validateSendCode(),
    UserController.sendCodeToEmail);

router.post('/confirm-code',
    UserController.validateConfirmVerifyCode(),
    UserController.resetPhoneConfirmVerifyCode);

router.post('/reset-phone',
    UserController.validateResetPhone(),
    UserController.resetPhone);

router.route('/getAll')
    .get(UserController.findAll)
router.route('/getUser')
    .get(requireAuth,UserController.getUser)
router.route('/:userId/delete')
    .delete(requireAuth,UserController.delete)
router.route('/addCoupon')
    .put(requireAuth,UserController.addCoupon)
router.route('/:userId/reduceBalance')
    .put(requireAuth,UserController.reduceBalance)
export default router;
